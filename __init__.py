# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .unit_load import UnitLoad


def register():
    Pool.register(
        UnitLoad,
        module='stock_unit_load_lot', type_='model')
