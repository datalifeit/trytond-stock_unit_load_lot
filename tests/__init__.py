# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_stock_unit_load_lot import suite

__all__ = ['suite']
